﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace _107IPHW_HW1
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		Image<Bgr, Byte> inputImage;

		byte b, g, r;

        /* :( */
		private void binarization_btn_Click(object sender, EventArgs e)
		{
            Image<Bgr, Byte> _Image = inputImage.Clone();
            const int THRESHOLD = 128;

            for (int x = 0; x < _Image.Rows; ++x)
            {
                for (int y = 0; y < _Image.Cols; ++y)
                {
                    b = _Image.Data[x, y, 0];
					g = _Image.Data[x, y, 1];
					r = _Image.Data[x, y, 2];


					byte grayScale = (byte)((b * 0.3) + (g * 0.59) + (r * 0.11));
                    if (grayScale >= THRESHOLD)
                        grayScale = 255;
                    else
                        grayScale = 0;

                    _Image.Data[x, y, 0] = grayScale;
                    _Image.Data[x, y, 1] = grayScale;
                    _Image.Data[x, y, 2] = grayScale;
                }
            }

            _outputPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            _outputPictureBox.Image = _Image.ToBitmap();
        }

		private void invert_btn_Click(object sender, EventArgs e)
		{
            Image<Bgr, Byte> _Image = inputImage.Clone();

            for (int x = 0; x < _Image.Rows; ++x)
            {
                for (int y = 0; y < _Image.Cols; ++y)
                {
                    b = _Image.Data[x, y, 0];
                    g = _Image.Data[x, y, 1];
                    r = _Image.Data[x, y, 2];

                    _Image.Data[x, y, 0] = (byte)(255 - b);
                    _Image.Data[x, y, 1] = (byte)(255 - g);
                    _Image.Data[x, y, 2] = (byte)(255 - r);
                }
            }

            _outputPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            _outputPictureBox.Image = _Image.ToBitmap();
        }

		private void relief_btn_Click(object sender, EventArgs e)
		{
            Image<Bgr, Byte> _Image = inputImage.Clone();

            int [,] relief_matrix = {{-2, -1, 0},
                                     {-1, 1, 1},
                                     {0, 1, 2}};

            for (int x = 0; x < _Image.Rows; ++x)
            {
                for (int y = 0; y < _Image.Cols; ++y)
                {
                    _Image.Data[x, y, 0] = convolution(relief_matrix, pixelMatrix(x, y, inputImage, 0));
                    _Image.Data[x, y, 1] = convolution(relief_matrix, pixelMatrix(x, y, inputImage, 1));
                    _Image.Data[x, y, 2] = convolution(relief_matrix, pixelMatrix(x, y, inputImage, 2));
                }
            }

            _outputPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            _outputPictureBox.Image = _Image.ToBitmap();
        }

        private byte[,] pixelMatrix (int x, int y, Image<Bgr, Byte> image, int bgrType)
        {
            byte[,] pixel = new byte[3, 3];

            for (int px_i = -1; px_i <= 1 && (x + px_i >= 0 && x + px_i < image.Rows); ++px_i)
            {
                for (int px_j = -1; px_j <= 1 && (y + px_j >= 0 && y + px_j < image.Cols); ++px_j)
                {
                    pixel[px_i + 1, px_j + 1] = image.Data[px_i + x, px_j + y, bgrType];
                }
            }

            return pixel;
        }

		private void sharpen_btn_Click(object sender, EventArgs e)
		{
            Image<Bgr, Byte> _Image = inputImage.Clone();

            int[,] sharpen_matrix = {{0, -1, 0},
                                     {-1, 5, -1},
                                     {0, -1, 0}};

            for (int x = 0; x < _Image.Rows; ++x)
            {
                for (int y = 0; y < _Image.Cols; ++y)
                {
                    _Image.Data[x, y, 0] = convolution(sharpen_matrix, pixelMatrix(x, y, inputImage, 0));
                    _Image.Data[x, y, 1] = convolution(sharpen_matrix, pixelMatrix(x, y, inputImage, 1));
                    _Image.Data[x, y, 2] = convolution(sharpen_matrix, pixelMatrix(x, y, inputImage, 2));
                }
            }

            _outputPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            _outputPictureBox.Image = _Image.ToBitmap();
        }

		private void mirror_btn_Click(object sender, EventArgs e)
		{
            Image<Bgr, Byte> _Image = inputImage.Clone();

            for (int x = 0; x < _Image.Rows; ++x)
            {
                for (int y = 0; y < _Image.Cols; ++y)
                {
                    _Image.Data[x, y, 0] = inputImage.Data[x, inputImage.Cols - y - 1, 0];
                    _Image.Data[x, y, 1] = inputImage.Data[x, inputImage.Cols - y - 1, 1];
                    _Image.Data[x, y, 2] = inputImage.Data[x, inputImage.Cols - y - 1, 2];
                }
            }

            _outputPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            _outputPictureBox.Image = _Image.ToBitmap();
        }

		private void open_image_btn_Click(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Title = "Select file";
			dialog.InitialDirectory = ".\\";
			dialog.Filter = "所有合適文件(*.bmp, *.jpg *.png) | *.bmp; *.jpg; *.png";

			if (dialog.ShowDialog() == DialogResult.OK)
			{
				inputImage = new Image<Bgr, byte>(dialog.FileName);

				_sourcePictureBox.SizeMode = PictureBoxSizeMode.Zoom;
				_sourcePictureBox.Image = inputImage.ToBitmap();

			}
		}

        /* Die method provided by TA :) */
		private void gray_btn_Click(object sender, EventArgs e)
		{
			Image<Bgr, Byte> _Image = inputImage.Clone();


			for (int x = 0; x < _Image.Rows; x++)
			{
				for (int y = 0; y < _Image.Cols; y++)
				{

					b = _Image.Data[x, y, 0];
					g = _Image.Data[x, y, 1];
					r = _Image.Data[x, y, 2];


					byte grayScale = (byte)((b * 0.3) + (g * 0.59) + (r * 0.11));

					_Image.Data[x, y, 0] = grayScale;
					_Image.Data[x, y, 1] = grayScale;
					_Image.Data[x, y, 2] = grayScale;


				}

			}
			_outputPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
			_outputPictureBox.Image = _Image.ToBitmap();

		}

        /* ( ͡° ͜ʖ ͡°) */
        private byte convolution (int [,] convert_matrix, byte [,] original)
        {
            int outcome = 0;
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    outcome += convert_matrix[i, j] * original[i, j];
                }
            }

            outcome = outcome < 0 ? 0 : outcome > 255 ? 255 : outcome;
            return (byte)outcome;
        }
	}
}
